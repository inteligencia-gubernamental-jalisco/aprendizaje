#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["Jorge Martínez Ortega"]
__copyright__ = "Copyright 2019, Gobierno de Jalisco"
__credits__ = ["Jorge Martínez Ortega"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = "Jorge Martínez Ortega"
__email__ = "jorge.martinez@jalisco.gob.mx"
__status__ = "Development"

import datetime
from sqlalchemy import create_engine, Column, Boolean, Integer, Float, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.engine.url import URL

import settings

Base = declarative_base()

def db_connect():
    """
    Performs database connection using database settings from settings.py.
    Returns sqlalchemy engine instance
    """
    return create_engine(URL(**settings.DATABASE))

def create_deals_table(engine):
    """"""
    Base.metadata.create_all(engine)

class DataModel(Base):
    """Sqlalchemy deals model"""
    __tablename__ = 'datos'

    id = Column(String, primary_key=True)

    primer_dato = Column(Float, default=None)
    segundo_dato = Column(Integer, default=None)
    tercer_dato = Column(String, default=None)