#!/usr/bin/python
# -*- coding: utf-8 -*-

__author__ = ["Jorge Martínez Ortega"]
__copyright__ = "Copyright 2019, Gobierno de Jalisco"
__credits__ = ["Jorge Martínez Ortega"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = "Jorge Martínez Ortega"
__email__ = "jorge.martinez@jalisco.gob.mx"
__status__ = "Development"

DATABASE = {
    'drivername': 'postgres',
    'host': 'dockercomposeexample_postgres_1',
    'port': '5432',
    'username': 'myuser',
    'password': 'mypassword',
    'database': 'datos'
}