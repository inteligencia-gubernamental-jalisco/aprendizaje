#!/usr/bin/env python
## -*- coding: utf-8 -*-

__author__ = ["Jorge Martínez Ortega"]
__copyright__ = "Copyright 2019, Gobierno de Jalisco"
__credits__ = ["Jorge Martínez Ortega"]
__license__ = "MIT"
__version__ = "0.0.1"
__maintainer__ = ["Jorge Martínez Ortega"]
__email__ = "jorge.martinez@jalisco.gob.mx"
__status__ = "Development"

from aiohttp import web
import sys
sys.path.append('./src')
from data_model import *

async def init(request):
    db=db_connect()
    Session = sessionmaker(db)
    session = Session()
    Base.metadata.create_all(db)
    return web.Response(text="DB init")

async def handle(request):
    name = request.match_info.get('name', "World!")
    text = "Hello, " + name		
    document = DataModel(id=name)
    db=db_connect()
    Session = sessionmaker(db)
    session = Session()
    r = session.merge(document)
    session.commit()
    print('received request, replying with "{}".'.format(text))
    return web.Response(text=text)

app = web.Application()
app.router.add_get('/init', init)
app.router.add_get('/', handle)
app.router.add_get('/{name}', handle)

web.run_app(app, port=5858)
