# Capacitaciones
* [git, Uso Básico y Buenas Prácticas](https://gitlab.com/inteligencia-gubernamental-jalisco/aprendizaje/wikis/Git,-Uso-B%C3%A1sico-y-Buenas-Pr%C3%A1cticas)
* [Uso Básico de Docker](https://gitlab.com/inteligencia-gubernamental-jalisco/aprendizaje/wikis/Uso-b%C3%A1sico-de-Docker)
* [Charla Geonode](https://docs.google.com/presentation/d/1klHwRycyzeiHc-RLr1zSptRNzqel7oESM3vSipx98IE/edit?usp=sharing)
* [En qué se puede usar el deep learning](https://docs.google.com/presentation/d/1Y67R3WO86acwo6K_zlANmhSfFQibiWgu4oQSEI76UJ8/edit?usp=sharing)
* [BASES AND LIMITS OF DEEP LEARNING](https://github.com/asp1420/c1)
* [Google Cloud Platform for IA](https://docs.google.com/presentation/d/1ImEt3sjFolkwpSg153vSruVmfJ300ng6xwnylb7VFDk)
* [Gcloud BAsics Wiki](https://gitlab.com/inteligencia-gubernamental-jalisco/aprendizaje/wikis/Gcloud-basics)
* [AWS Basico](https://gitlab.com/inteligencia-gubernamental-jalisco/aprendizaje/wikis/AWS/AWS-Basics)
* [AWS 2](https://gitlab.com/inteligencia-gubernamental-jalisco/aprendizaje/wikis/AWS/AWS-Console)
* [Papers interesantes](https://gitlab.com/inteligencia-gubernamental-jalisco/aprendizaje/-/wikis/Papers-interesantes)
# Material

* [CIFAR-Deep-Learning-Reinforcement-Learning-Summer-School-2020](https://gitlab.com/inteligencia-gubernamental-jalisco/aprendizaje/-/wikis/CIFAR-Deep-Learning---Reinforcement-Learning-Summer-School-2020)

* [Curso completo de python 2 años](https://github.com/ossu/computer-science#summary)
* [Machine Learning for Remote Sensing: Agriculture and Food Security: CVPR 2022](https://gitlab.com/inteligencia-gubernamental-jalisco/aprendizaje/-/wikis/Machine-Learning-for-Remote-Sensing:-Agriculture-and-Food-Security:-CVPR-2022)
